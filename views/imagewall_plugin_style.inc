<?php

/**
 * @file
 * Draggableviews style plugin definition.
 */

/**
 * Style plugin to render each item as a container with objects (fields);
 */
class imagewall_plugin_style extends views_plugin_style {

  function validate() {
    $errors = parent::validate();

    $fields = $this->view->get_items('field', $this->display->id);

    // Check if all configured fields are available.
    $title_field = $this->options['general']['title']['field'];
    if (!isset($title_field) || !isset($fields[$title_field])) {
      $errors[] = t('Display "@display": Imagewall: The selected title field is not valid. Check your settings.', array('@display' => $this->display->display_title));
    }
    $group_by_field = $this->options['general']['group_by']['field'];
    if (!isset($group_by_field) || !isset($fields[$group_by_field])) {
      $errors[] = t('Display "@display": Imagewall: The selected group field is not valid. Check your settings.', array('@display' => $this->display->display_title));
    }

    return $errors;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    // set theme handler
    // theme function is registered in *.module file
    $form['#theme'] = 'imagewall_ui_style_plugin';

    // get field handler
    $handlers = $this->display->handler->get_handlers('field');

    $fields = array();
    foreach ($handlers as $field => $handler) {
      if ($label = $handler->label()) {
        $fields[$field] = $label;
      }
      else {
        $fields[$field] = $handler->ui_name();
      }
    }

    $form['general'] = array(
      'title' => array(
        'label' => array(
          '#type' => 'markup',
          '#value' => t('Title of Containers'),
        ),
        'field' => array(
          '#type' => 'select',
          '#options' => $fields,
          '#default_value' => isset($this->options['general']['title']['field']) ? $this->options['general']['title']['field'] : key($fields),
        ),
      ),
      'group_by' => array(
        'label' => array(
          '#type' => 'markup',
          '#value' => t('Group containers by this field (CSS)'),
        ),
        'field' => array(
          '#type' => 'select',
          '#options' => $fields,
          '#default_value' => isset($this->options['general']['group_by']['field']) ? $this->options['general']['group_by']['field'] : key($fields),
        ),
      ),
    );
    $form['lock'] = array(
      '#type' => 'checkboxes',
      '#name' => 'lock',
      '#options' => array('lock' => t('The structure cannot be changed (locked)')),
      '#title' => t('Lock the structure'),
      '#default_value' => isset($this->options['lock']) ? $this->options['lock'] : array(),
    );
  }

  /**
   * Render the display in this style.
   */
  function render() {
    if (strcmp($this->options['lock']['lock'], 'lock') == 0) {
      return theme('imagewall_view_locked', $this->view, $this->options);
    }
    return theme('imagewall_view', $this->view, $this->options);
  }
}
