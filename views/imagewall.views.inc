<?php

/**
 * @file
 * Derives the view style plugin
 */

/**
 * Implemening hook_views_plugins
 */
function imagewall_views_plugins() {
  return array(
    'module' => 'imagewall', // This just tells our themes are elsewhere.

    'style' => array(
      'imagewall' => array(
        'path' => drupal_get_path('module', 'imagewall') .'/views',
        'title' => t('Draggable Objects'),
        'help' => t('Draggable Objects.'),
        'handler' => 'imagewall_plugin_style',
        'theme' => 'imagewall_view',
        'theme file' => 'imagewall_theme.inc',
        'uses row plugin' => FALSE,
        'uses options' => TRUE,
        'uses fields' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}

function imagewall_views_pre_build(&$view) {
  if ($view->display_handler->get_option('style_plugin') != 'imagewall') {
    // This view/display doesn't use imagewall style plugin. Nothing to do.
    return;
  }

  // Make arguments available for further operations.
  if (empty($view->args)) {
    $args = 'none';
  }
  else {
    $args = implode(',', $view->args);
  }
  $view->imagewall['real_args'] = $args;

  // @todo: HACK: Set latest svid if nothing selected.
  if (!isset($_REQUEST['svid'])) {
    // Get the node id that represents this structure.
  	$res = db_query("SELECT nid FROM {imagewall_nodes} WHERE view_id=%d AND view_arg='%s'", $view->vid, $args);
    $item = db_fetch_object($res);
    $nid = $item->nid;
    // No get current revision of this node. This revision represents the structure version id (svid).
  	$res = db_query("SELECT vid AS svid FROM {node} WHERE nid='%d'", $nid);
    $item = db_fetch_object($res);

    // Change the global variable.
    $_REQUEST['svid'] = $item->svid;
  }

  $svid = $_REQUEST['svid'];

  // @todo: HACK: Remove argument value and add new argument.
  if (isset($svid)) {
    // Select base Ids from containers only.
    $res = db_query("SELECT bid FROM {imagewall_structure} WHERE svid=%d AND field_name='container'", $svid);
    $bids = array();
    while ($item = db_fetch_object($res)) {
      $bids[] = $item->bid;
    }
    unset($view->args[0]);

    $view->display_handler->set_option('arguments', array(
      'vid' => array(
        'default_action' => 'default',
        'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => 'All',
    'title' => '',
    'default_argument_type' => 'fixed',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'break_phrase' => 1,
    'not' => 0,
    'id' => 'vid',
    'table' => 'node_revisions',
    'field' => 'vid',
    'relationship' => 'none',
    'default_options_div_prefix' => '',
    'default_argument_user' => 0,
    'default_argument_fixed' => implode(',', $bids),
    'default_argument_php' => '',
    'validate_argument_node_type' => array(
      'image' => 0,
      'object' => 0,
      'page' => 0,
      'story' => 0,
    ),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(),
    'validate_argument_type' => 'tid',
    'user_argument_type' => '',
    'restrict_user_roles' => 0,
    'user_roles' => array(),
    'validate_argument_php' => '',
      ),
    ));
  }
}