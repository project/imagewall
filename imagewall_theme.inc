<?php

/**
 * @file
 * Theme functions.
 */

function template_preprocess_imagewall_view(&$vars) {
  $view         = $vars['view'];
  $view_args    = $view->imagewall['real_args'];
  $path         = implode('/', arg());
  $options      = $view->style_plugin->options;
  $svid         = $_REQUEST['svid'];
  $title_field  = $options['general']['title']['field'];
  $group_by     = $options['general']['group_by']['field'];

  // In order to build the revisions list we need to find out the representing node ID.
  $res = db_query("SELECT nid FROM {imagewall_nodes} WHERE view_id=%d AND view_arg='%s'", $view->vid, $view_args);
  $item = db_fetch_object($res);
  $nid = $item->nid;
  // If the node exists, get revisions list.
  $revisions_list = array();
  if (!empty($nid)) {
    $node = new StdClass();
    $node->nid = $nid;
    $revisions_list = node_revision_list($node);
  }
  // Prepare revisions list for output.
  $revisions = array();
  foreach ($revisions_list AS $vid => $rev) {
    $revisions[] = array(
      'svid'   => $vid,
      'string' => imagewall_revision_string($rev),
      'title'  => imagewall_revision_title($rev),
    );
  }

  // Default settings.
  $default_settings = array(
    'container' => array(
      'box' => array(
        'canvas' => FALSE,
        'x'      => 0,
        'y'      => 0,
        'z'      => 0,
        'width'  => false,
        'height' => 15,
      ),
      'canvas' => array(
        'width'  => 130,
        'height' => 100,
      ),
    ),
    'object' => array(
      'canvas' => array(
        'x'      => 0,
        'y'      => 20,
        'z'      => 10,
        'width'  => 100,
        'height' => 20,
        'x_increase' => 60,
        'y_increase' => 20,
      ),
    )
  );

  // Build containers.
  $containers = array();
  foreach ($view->result AS $i => $result) {
    // The base-ID.
    $bid = $result->{$view->base_field};
    // The group.
    $group = $result->{$view->field[$group_by]->field_alias};

    if (isset($title_field)) {
      $title = substr($view->field[$title_field]->render($result), 0, 15);
    }
    else {
      $title = 'Node'. ($i + 1);
    }

    // Check if this node revision is the latest version of the current node.
    $latest = FALSE;
    $res  = db_query("SELECT nid FROM {node} WHERE vid=%d", $result->{$view->base_field});
    $item = db_fetch_object($res);
    if (!empty($item->nid)) {
      $latest = TRUE;
    }

    // Get all term ids that are attached to the current node.
    $term_ids = array();
    $res  = db_query("SELECT tid FROM {term_node} WHERE vid=%d", $bid);
    while( $item = db_fetch_object($res)) {
      $term_ids[] = $item->tid;
    }

    // Get state of container.
    $res   = db_query("SELECT canvas, x, y, z, width, height FROM {imagewall_structure} WHERE svid=%d AND bid=%d AND field_name='%s'", $svid, $bid, 'container');
    $state = db_fetch_object($res);

    $container_id = 'imagewall-container-'. $bid;
    $containers[$container_id] = array(
      'bid'    => $bid,
      'title'  => $title,
      'terms'  => $term_ids,
      'latest' => $latest,
      'group'  => $result->node_type,
      'state'   => array(
        'canvas' => $state->canvas ? $state->canvas : FALSE,
        'x'      => $state->canvas ? $state->x : $default_settings['container']['box']['x'],
        'y'      => $state->canvas ? $state->y : $default_settings['container']['box']['y'],
        'z'      => $state->canvas ? $state->z : $default_settings['container']['box']['z'],
        'width'  => $state->width ? $state->width : 50,
        'height' => $state->height ? $state->height : 50,
      ),
    );

    // Loop through all fields of this node.
    foreach ($view->field AS $field) {
      if ($field->options['exclude']) {
        // Skip type field if its exluded from display.
        continue;
      }

      // Get state of object.
      $res   = db_query("SELECT canvas, x, y, z, width, height FROM {imagewall_structure} WHERE svid=%d AND bid=%d AND field_name='%s'", $svid, $bid, $field->real_field);
      $state = db_fetch_object($res);

      $object_id = $container_id .'-field-'. str_replace('_', '-', $field->real_field);
      $containers[$container_id]['objects'][$object_id] = array(
        'label'      => $field->options['label'],
        'content'    => $field->render($result),
        'field_name' => $field->real_field,
        'state'   => array(
          'canvas' => $state->canvas,
          'x'      => $state->x ? $state->x : $default_settings['object']['canvas']['x'],
          'y'      => $state->y ? $state->y : $default_settings['object']['canvas']['y'],
          'z'      => $state->z ? $state->z : $default_settings['object']['canvas']['z'],
          'width'  => $state->width ? $state->width : $default_settings['object']['canvas']['width'],
          'height' => $state->height ? $state->height : $default_settings['object']['canvas']['height'],
        ),
      );
    }
  }

  // Add all needed javascripts and stylesheets.
  jquery_ui_add('ui.draggable');
  jquery_ui_add('ui.droppable');
  jquery_ui_add('ui.resizable');
  jquery_ui_add('ui.selectable');
  drupal_add_js(drupal_get_path('module', 'imagewall') .'/imagewall.js', 'module');
  drupal_add_css(drupal_get_path('module', 'imagewall') .'/styles.css');

  // Export all information to javascript.
  drupal_add_js(array(
    'imagewall' => array(
      'vid'                          => $view->vid,
      'view_name'                    => $view->name,
      'view_display'                 => $view->current_display,
      'view_args'                    => $view_args,
      'use_revisions'                => TRUE,
      'path'                         => url($path),
      'ajax_save_path'               => url('imagewall/ajax_save_canvas'),
      'ajax_retrieve_revisions_path' => url('imagewall/ajax_retrieve_revisions'),
      'ajax_load_revision_path'      => url('imagewall/ajax_load_revision'),
      'containers'                   => $containers,
      'title_field'                  => $title_field,
      'preview'                      => false,
      'default_settings'             => $default_settings,
    ),
  ),
  'setting');

  $submit_form = array(
    'log' => array(
      '#description' => t('Log message'),
      '#type' => 'textfield',
      '#id'   => 'imagewall-log',
      '#value' => '',
      '#maxlength' => 255,
      '#weight' => 1,
    ),
    'button' => array(
      '#type' => 'button',
      '#id'   => 'imagewall-submit',
      '#attributes' => array('onclick' => 'imagewall_save();'),
      '#value' => t('Submit'),
      '#weight' => 2,
    ),
  );

  $vars['revisions']   = $revisions;
  $vars['containers']  = $containers;
  $vars['submit_form'] = drupal_render($submit_form);

  // Include css of theme folder.
  global $theme_key;
  drupal_add_css(drupal_get_path('theme', $theme_key) .'/imagewall.css', 'theme');
  drupal_add_css(drupal_get_path('theme', $theme_key) .'/imagewall-'. $view_args .'.css', 'theme');
}

function template_preprocess_imagewall_view_locked(&$vars) {
  $view         = $vars['view'];
  $view_args    = $view->imagewall['real_args'];
  $options      = $view->style_plugin->options;
  $svid         = $_REQUEST['svid'];
  $title_field  = $options['general']['title']['field'];
  $group_by     = $options['general']['group_by']['field'];

  // Default settings.
  $default_settings = array(
    'container' => array(
      'box' => array(
        'canvas' => FALSE,
        'x'      => 0,
        'y'      => 0,
        'z'      => 0,
        'width'  => false,
        'height' => 15,
      ),
      'canvas' => array(
        'width'  => 130,
        'height' => 100,
      ),
    ),
    'object' => array(
      'canvas' => array(
        'x'      => 0,
        'y'      => 20,
        'z'      => 10,
        'width'  => 100,
        'height' => 20,
        'x_increase' => 60,
        'y_increase' => 20,
      ),
    )
  );

  // Build containers.
  $containers = array();
  foreach ($view->result AS $i => $result) {
    // The base-ID.
    $bid = $result->{$view->base_field};
    // The group.
    $group = $result->{$view->field[$group_by]->field_alias};

    if (isset($title_field)) {
      $title = substr($view->field[$title_field]->render($result), 0, 15);
    }
    else {
      $title = 'Node'. ($i + 1);
    }

    // Check if this node revision is the latest version of the current node.
    $latest = FALSE;
    $res  = db_query("SELECT nid FROM {node} WHERE vid=%d", $result->{$view->base_field});
    $item = db_fetch_object($res);
    if (!empty($item->nid)) {
      $latest = TRUE;
    }

    // Get all term ids that are attached to the current node.
    $term_ids = array();
    $res  = db_query("SELECT tid FROM {term_node} WHERE vid=%d", $bid);
    while( $item = db_fetch_object($res)) {
      $term_ids[] = $item->tid;
    }

    // Get state of container.
    $res   = db_query("SELECT canvas, x, y, z, width, height FROM {imagewall_structure} WHERE svid=%d AND bid=%d AND field_name='%s'", $svid, $bid, 'container');
    $state = db_fetch_object($res);

    $container_id = 'imagewall-container-'. $bid;
    $containers[$container_id] = array(
      'bid'    => $bid,
      'title'  => $title,
      'terms'  => $term_ids,
      'latest' => $latest,
      'group'  => $result->node_type,
      'state'   => array(
        'canvas' => $state->canvas ? $state->canvas : FALSE,
        'x'      => $state->canvas ? $state->x : $default_settings['container']['box']['x'],
        'y'      => $state->canvas ? $state->y : $default_settings['container']['box']['y'],
        'z'      => $state->canvas ? $state->z : $default_settings['container']['box']['z'],
        'width'  => $state->width ? $state->width : 50,
        'height' => $state->height ? $state->height : 50,
      ),
    );

    // Loop through all fields of this node.
    foreach ($view->field AS $field) {
      if ($field->options['exclude']) {
        // Skip type field if its exluded from display.
        continue;
      }

      // Get state of object.
      $res   = db_query("SELECT canvas, x, y, z, width, height FROM {imagewall_structure} WHERE svid=%d AND bid=%d AND field_name='%s'", $svid, $bid, $field->real_field);
      $state = db_fetch_object($res);

      $object_id = $container_id .'-field-'. str_replace('_', '-', $field->real_field);
      $containers[$container_id]['objects'][$object_id] = array(
        'label'      => $field->options['label'],
        'content'    => $field->render($result),
        'field_name' => $field->real_field,
        'state'   => array(
          'canvas' => $state->canvas,
          'x'      => $state->x ? $state->x : $default_settings['object']['canvas']['x'],
          'y'      => $state->y ? $state->y : $default_settings['object']['canvas']['y'],
          'z'      => $state->z ? $state->z : $default_settings['object']['canvas']['z'],
          'width'  => $state->width ? $state->width : $default_settings['object']['canvas']['width'],
          'height' => $state->height ? $state->height : $default_settings['object']['canvas']['height'],
        ),
      );
    }
  }

  // Add all needed javascripts and stylesheets.
  drupal_add_js(drupal_get_path('module', 'imagewall') .'/imagewall_locked.js', 'module');
  drupal_add_css(drupal_get_path('module', 'imagewall') .'/styles.css');

  // Export all information to javascript.
  drupal_add_js(array(
    'imagewall' => array(
      'use_revisions'                => TRUE,
      'containers'                   => $containers,
      'title_field'                  => $title_field,
      'preview'                      => TRUE,
      'default_settings'             => $default_settings,
    ),
  ),
  'setting');

  $vars['containers']  = $containers;

  // Include css of theme folder.
  global $theme_key;
  drupal_add_css(drupal_get_path('theme', $theme_key) .'/imagewall.css', 'theme');
  drupal_add_css(drupal_get_path('theme', $theme_key) .'/imagewall-'. $view_args .'.css', 'theme');
  drupal_add_css(drupal_get_path('theme', $theme_key) .'/imagewall-'. $view_args .'-preview.css', 'theme');
}

/**
 * Theme the form for the table style plugin
 */
function theme_imagewall_ui_style_plugin($form) {
  // Build table:
  $rows = array();
  // Position.
  foreach (element_children($form['general']) AS $id) {
    $columns = array();
    foreach (element_children($form['general'][$id]) AS $col) {
      $columns[] = drupal_render($form['general'][$id][$col]);
    }
    $rows[] = $columns;
  }

  $output .= theme('table', array('&nbsp;', t('Field')), $rows);
  $output .= drupal_render($form['lock']);
  return $output;
}

/*
 * filter handlers by type
 *
 * @param $type
 *   the field type
 * @param $fields
 *   the fields array
 * return
 *   the available fields
 */
function _imagewall_filter_fields($types = array(''), $handlers) {
  $available_fields = array();

  foreach ($handlers as $field => $handler) {
    if ($label = $handler->label()) {
      $available_fields[$field] = $label;
    }
    else {
      $available_fields[$field] = $handler->ui_name();
    }
  }
  return $available_fields;
}