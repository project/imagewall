// Check js availability.
if (Drupal.jsEnabled) {
  // Start at onload-event.
  $(document).ready(imagewall_load);
}

/**
 * Load containers and objects.
 */
function imagewall_load() {
  $imagewall_board = $("#imagewall-canvas");
  $imagewall_box   = $("#imagewall-box");
  
  if (Drupal.settings.imagewall.containers) {
    var containers = Drupal.settings.imagewall.containers;

    for (var container_id in containers) {
      var $container = $('#' + container_id);

      $container.draggable({
        revert: 'invalid', // when not dropped, the item will revert back to its initial position
        cursor: 'move',
        handle: 'div.imagewall-container-drag-handle'
      });

      // States.
      var container  = Drupal.settings.imagewall.containers[container_id];
      var state = container.state;
      
      if(state.canvas == true) {
        imagewall_attach_to_canvas($container, true);
        imagewall_init_state($container, state);
      }
    }

    $imagewall_board
    .droppable({
      hoverClass: 'imagewall-canvas-hover'
    })
    .selectable({
      filter: '.imagewall-container-drag-handle',
      cancel: '.imagewall-canvas-object, .imagewall-container-drag-handle, .ui-resizable-se, .imagewall-ui-resizable-se',
      stop: imagewall_selected_items
    });

    $imagewall_box.droppable({
      accept:     '.imagewall-canvas-container',
      hoverClass: 'imagewall-box-hover'
    });

    $imagewall_board.bind('drop', imagewall_drop_canvas);
    $imagewall_box.bind('drop', imagewall_drop_box);
  }
}

/**
 * Items get dropped in the box.
 */
function imagewall_drop_box(event, ui) {
  var $container = $(ui.draggable);

  if ($container.parent().get(0).id != 'imagewall-box') {
    // Reset container.
    var default_settings = Drupal.settings.imagewall.default_settings;
    var container = Drupal.settings.imagewall.containers[$container.get(0).id];

    $container
    .appendTo($imagewall_box)
    .resizable("destroy")
    .removeClass('imagewall-canvas-container')
    .addClass('imagewall-box-container')
    .css({
      'position': 'relative',
      'width':    null,
      'height':   null,
      'left':     null,
      'top':      null
    });

    imagewall_container_remove_templates($container, container);

    var objects = container['objects'];
    for (var object_id in objects) {
      var object = objects[object_id];
      var $obj = $('#' + object_id);
      $obj
      .fadeOut()
      .resizable('destroy')
      .draggable('destroy')
      .removeClass('imagewall-canvas-object-' + object.field_name + '-tag-' + Drupal.settings.imagewall.view_args);
      imagewall_object_remove_templates($obj, container, object);
    }
    
    // Just remember that the container is in the box. The contained objects don't have a choice.
    imagewall_process($container, 0);
  }
}

/**
 * Items get dropped on the canvas.
 */
function imagewall_drop_canvas(event, ui) {
  var $item = $(ui.draggable);
  if ($item.parent().get(0).id != 'imagewall-canvas') {
    if (Drupal.settings.imagewall.containers[$item.get(0).id]) {
      // @todo: Probably add here some code to prevent the jump of the items when they are dragged on the canvas.
      /*$item.css('left', $imagewall_board.position().left);
      var item_left = parseInt($item.css('left').substr(0, $item.css('left').indexOf('px')));
      var item_top = parseInt($item.css('left').substr(0, $item.css('top').indexOf('px')));
      var board_left = parseInt($imagewall_board.position().left);
      var box_left = parseInt($imagewall_box.position().left);
      var board_top = parseInt($imagewall_board.position().top);
      var mouse_left = event.pageX;
      var mouse_top  = event.pageY;*/
      // HACK:
      $item.css('left', (event.pageX - $imagewall_board.offset().left + 70) + 'px');
      $item.css('top', (event.pageY - $imagewall_board.offset().top + 70) + 'px');
    }
  }
  imagewall_attach_to_canvas($(ui.draggable), false);
}

/**
 * A generic item has been dropped on the canvas.
 
 * This function differs between container and object.
 */
function imagewall_attach_to_canvas($item, init) {
  var container = Drupal.settings.imagewall.containers[$item.get(0).id];

  if (container) {
    // A container has been dropped.
    var default_settings = Drupal.settings.imagewall.default_settings;
    var $container = $item;

    if ($container.parent().get(0).id != 'imagewall-canvas') {
      // The container comes from the box.

      $container
      // Change position.
      .css({
        'position': 'absolute',
      })
      .appendTo($imagewall_board)
      // Change behaviour.
      .draggable('option', 'stack', {group: '#imagewall-canvas div', min: 1})
      .draggable('options', 'opacity', 0.9)
      .resizable({handles: 'se'})
      // Change css class.
      .removeClass('imagewall-box-container')
      .addClass('imagewall-canvas-container')
      .css({
        'width':     null,
        'height':    null
      });

      // Assign templates.
      imagewall_container_assign_templates($container, container);

      $container.bind('resizestop', imagewall_resize_stop);

      // Prepare all contained objects and reset its states.
      var objects = container['objects'];
      /*
      var default_x = default_settings.object.canvas.x;
      var default_y = default_settings.object.canvas.y;
      */
      for (var object_id in objects) {
        var obj   = objects[object_id];
        var state = obj.state;
        var $obj  = $('#' + object_id);

        if (init == false) {
          // Reset object.
          state = {
            'canvas': true,
            'x'     : false,//default_x,
            'y'     : false,//default_y,
            'z'     : false,//default_settings.object.canvas.z,
            'width' : false,//default_settings.object.canvas.width,
            'height': false//default_settings.object.canvas.height
          };
          /*default_x += default_settings.object.canvas.x_increase;
          default_y += default_settings.object.canvas.y_increase;*/
        }

        // Assign template before calculating ratio and before initialization.
        imagewall_object_assign_templates($obj, container, obj);

        // Set ratio if object contains an image.
        var ratio = false;
        $obj.find('img:first').each(function(i) {
          // @todo: IE always says width=height=0. Thus ratio will be = NaN.
          /*ratio = ($(this).attr('width') / $(this).attr('height'));
          if (init == false) {
            var height = state.width / ratio;
            if (height > default_settings.object.canvas.height) {
              height = default_settings.object.canvas.height;
              $obj.css('width', height * ratio);
              $(this).css('width', height * ratio);
            }
            $(this).css('height', height);
            // Set state because dimensions changed.
            imagewall_set_state($obj, obj, 1);
          }*/
          var width, height;
          if ($obj.css('width')) width = parseInt($obj.css('width'));
          else                   width = 0;
          if ($obj.css('height')) height = parseInt($obj.css('height'));
          else                    height = 0;
          if (width > 0 && height > 0) {
            ratio = width / height;
          }
        });

        // Initialize state.
        imagewall_init_state($obj, state);

        $obj
        .draggable({
          //containment: $container,
          revert: 'invalid'
          })
        .draggable('option', 'stack', {group: '#imagewall-canvas div', min: 1})
        .resizable({
          alsoResize: $obj.find('img'),
          handles: 'se'
          //aspectRatio: ratio
          })
        .fadeIn();

        $obj.bind('resizestop', imagewall_resize_stop);
        $obj.bind('resize', imagewall_resize);
      }

      //$('.ui-resizable-e').addClass('imagewall-ui-resizable-e');
      //$('.ui-resizable-s').addClass('imagewall-ui-resizable-s');
      $('.ui-resizable-se').addClass('imagewall-ui-resizable-se');
    }
  }

  if (init == false) {
    // If this isn't the startup initialization process this item.
    imagewall_process($item, 1);
  }
}

function imagewall_resize_stop(event, ui) {
  var $item = ui.helper;
  imagewall_process($item, 1);
}

function imagewall_resize(event, ui) {
  if(!ui) {
    // Sometimes ui is NULL. This would cause an error in IE.
    return;
  }
  /*
  var $item = $(ui.helper);
  if ($item.find('img').size() == 0) {
    $item.css('font-size', (ui.size.height) + 'px');
  }
  */
}

/**
 * Process a generic item (both container and object).
 * Set the current state locally.
 */
function imagewall_process($item, canvas) {
  var field_name = '';
  var item = Drupal.settings.imagewall.containers[$item.get(0).id];

  if (item) {
    // The item currently processed is a container.
    bid        = item.bid;
    field_name = 'container';
  }
  else {
    // The item currently processed is an object.
    var object_id = $item.get(0).id;

    // Object-ID:    id="imagewall-CID-field-FIELD"
    // Container-ID: id="imagewall-CID"
    var container_id_length = object_id.indexOf('field');
    var container_id        = object_id.substr(0, container_id_length - 1);
    var container           = Drupal.settings.imagewall.containers[container_id];

    bid        = container.bid;
    item       = container['objects'][object_id];
    field_name = item.field_name;
  }

  imagewall_set_state($item, item, canvas);
}

function imagewall_set_state($item, item, canvas) {
  // Set the state locally.
  item.state = {
    'canvas': canvas,
    'x'     : $item.css('left'),
    'y'     : $item.css('top'),
    'z'     : $item.css('z-index'),
    'width' : $item.css('width'),
    'height': $item.css('height')
  };
}

/**
 * Initialize the current state.
 */
function imagewall_init_state($obj, state) {
  // Initialize with current states.
  $obj
  .css('left', state.x !== false ? (state.x + 'px') : null)
  .css('top', state.y !== false ? (state.y + 'px') : null)
  .css('z-index', state.z !== false ? (state.z + 'px') : null)
  .css('width', state.width !== false ? (state.width + 'px') : null)
  .css('height', state.height !== false ? (state.height + 'px') : null);

  if (Drupal.settings.imagewall.containers[$obj.get(0).id]) {
    // A container.

  }
  else {
    // An object.

    // Initialize dimensions.
    $obj.find('img')
    .css('width', $obj.css('width'))
    .css('height', $obj.css('height'));

    // Initialize text size.
    /*
    if ($obj.find('img').size() == 0) {
      $obj
      .css('font-size', state.height + 'px');
    }
    */
  }
}

/**
 * Items have been selected.
 */
function imagewall_selected_items() {
  $('#imagewall-selected-items li').each(function(i) {
    $(this).remove();
  });
  // All selected containes.
  $(".ui-selected.imagewall-container-drag-handle").each(function(selected_index) {
    var container_id = $(this).parent().attr('id');
    var containers = Drupal.settings.imagewall.containers;
    var container  = containers[container_id];
    
    if (Drupal.settings.imagewall.use_revisions == true) {
      // Build element ids.
      var form_id   = 'imagewall-node-revisions' + selected_index;
      var select_id = form_id + '-select';
      var li_id     = form_id + '-li';

      // Build html list element.
      var $li = $('<li id="' + li_id + '"></li>');
      $li.appendTo('#imagewall-selected-items');

      imagewall_loading($li, true);

      // Get all available revisions for this node.
      $.post(Drupal.settings.imagewall.ajax_retrieve_revisions_path, {vid: container.bid},
        function(data) {
          // Build html select element.
          var html_select = '<select id="' + select_id + '">';
          for (var i = 0; i < data.revisions.length; i++) {
            html_select += '<option value="' + data.revisions[i].vid + '" ' + (data.revisions[i].vid == container.bid ? ' selected="selected"' : '') + '>' + data.revisions[i].string + '</options>';
          }
          html_select += '</select>';
          var $select = $(html_select);

          // Build html button.
          var $button = $('<input type="button" value="Load">');

          $button.bind('click', function(e) {
            // Load node revision.
            var selected_node_vid = $('#' + select_id + ' option:selected').attr('value');
            // This revision must not already beeing used.
            for (var temp_container_id in containers) {
              if (containers[temp_container_id].bid == selected_node_vid) {
                // This node vid is already beeing used.
                alert(Drupal.t('This node version is already beeing used. Please choose another version.'));
                return;
              }
            }

            $.post(Drupal.settings.imagewall.ajax_load_revision_path,
            { vid:          selected_node_vid,
              view_name:    Drupal.settings.imagewall.view_name,
              view_display: Drupal.settings.imagewall.view_display
            },
              function(data) {
                var node = data['node'];
                // Change the base table ID to the new revision ID.
                container.bid = selected_node_vid;
                container.latest = data.latest;

                // Change title of drag handle.
                $('#' + container_id + ' .imagewall-container-drag-handle').html(node[Drupal.settings.imagewall.title_field]);
                if (data.latest == true) {
                  $('#' + container_id + ' .imagewall-container-drag-handle').removeClass('imagewall-not-up-to-date');
                }
                else {
                  $('#' + container_id + ' .imagewall-container-drag-handle').addClass('imagewall-not-up-to-date');
                }

                // Change field data with new information.
                for (var object_id in container['objects']) {
                  var object = container['objects'][object_id];
                  $('#' + object_id).html(node[object.field_name]);
                }
                // Remove form from list.
                $('#' + li_id).remove();
              }, 'json');

              imagewall_loading($('#' + li_id), true);
          });

          imagewall_loading($li, false);

          // Append html elements to selected items list.
          $li
          .append('<span>' + '<strong>' + container.title + '</strong><br />' + Drupal.t('Change revision to') + ':</span>')
          .append($select)
          .append($button);
        }, 'json');
    }
  });
}

/**
 * Save the current state of all items to the database.
 */
function imagewall_save() {
  var containers = Drupal.settings.imagewall.containers;
  var data = new Object();

  data['view_id']   = Drupal.settings.imagewall.vid;
  data['view_name'] = Drupal.settings.imagewall.view_name;
  data['view_args'] = Drupal.settings.imagewall.view_args;
  data['log']       = $('#imagewall-log').val();

  for (var container_id in containers) {
    var container = containers[container_id];
    // Refresh state before saving. E.g. z-index might have been changed of elements which haven't been moved.
    imagewall_set_state($('#' + container_id), container, container.state.canvas);

    var key = 'containers[' + container.bid + ']';

    for (var prop in container.state) {
      data[key + '[state][' + prop + ']'] = container.state[prop];
    }

    for (var object_id in container['objects']) {
      var object = container['objects'][object_id];
      // Refresh state.
      imagewall_set_state($('#' + object_id), object, object.state.canvas);
      for (var prop in object.state) {
        data[key + '[objects][' + object.field_name + '][state][' + prop + ']'] = object.state[prop];
      }
    }
  }

  // Save canvas.
  imagewall_loading($('#imagewall-submit-loading'), true);
  $('#imagewall-submit').attr('disabled', 'disabled');
  $.post(Drupal.settings.imagewall.ajax_save_path,
         data,
         function(data) {
           imagewall_attach_revision(data);
           imagewall_loading($('#imagewall-submit-loading'), false);
           $('#imagewall-submit').attr('disabled', null);
         }, 'json');
}

function imagewall_attach_revision(data) {
  $('#imagewall-revisions')
  .prepend('<option value="' + data.svid + '" title="' + data.title + '">' + data.string + '</option>')
  .find('option:first').attr('selected', 'selected');
}

function imagewall_load_structure_revision() {
  var select = $('#imagewall-revisions').get(0);
  var svid = select.options[select.selectedIndex].value;
  window.location.href = Drupal.settings.imagewall.path + '&svid=' + svid;
}

function imagewall_loading($obj, status) {
  if (status) {
    $obj.html('Loading..');
    $obj.addClass('imagewall-throbbing');
  }
  else {
    $obj.removeClass('imagewall-throbbing');
    $obj.html('');
  }
}

function imagewall_preview() {
  preview = Drupal.settings.imagewall.preview;
  if (preview) {
    $('.imagewall-container-drag-handle').css('display', 'inherit');
    $('.imagewall-ui-resizable-se').css('display', 'inherit');
    $('#imagewall-preview').html('Preview ON');
    Drupal.settings.imagewall.preview = false;
  }
  else {
    $('.imagewall-container-drag-handle').css('display', 'none');
    $('.imagewall-ui-resizable-se').css('display', 'none');
    $('#imagewall-preview').html('Preview OFF');
    Drupal.settings.imagewall.preview = true;
  }
}

function imagewall_object_assign_templates($obj, container, obj) {
  $obj.addClass('imagewall-canvas-object-' + obj.field_name);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.addClass('imagewall-term-' + container.terms[i]);
  }
}

function imagewall_object_remove_templates($obj, container, obj) {
  $obj.removeClass('imagewall-canvas-object-' + obj.field_name);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.removeClass('imagewall-term-' + container.terms[i]);
  }
}

function imagewall_container_assign_templates($obj, container) {
  $obj.addClass('imagewall-canvas-container-' + container.group);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.addClass('imagewall-term-' + container.terms[i]);
  }
  // Remove template from box.
  $obj.removeClass('imagewall-box-container-' + container.group);
}

function imagewall_container_remove_templates($obj, container) {
  $obj.removeClass('imagewall-canvas-container-' + container.group);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.removeClass('imagewall-term-' + container.terms[i]);
  }
  // Assign template for box.
  $obj.addClass('imagewall-box-container-' + container.group);
}

/**
 * Generate template of current states.
 */
function imagewall_generate_template() {
  var containers = Drupal.settings.imagewall.containers;

  var output_array = new Array();

  for (var container_id in containers) {
    var container = containers[container_id];
    if (container.state.canvas == false) {
      // Skip settings if container is not on canvas.
      continue;
    }

    // Refresh state before saving. E.g. z-index might have been changed of elements which haven't been moved.
    imagewall_set_state($('#' + container_id), container, container.state.canvas);

    // Template for container.
    var class_name = '.imagewall-canvas-container-' + container.group;
    for (var i = 0; i < container.terms.length; i++) {
      class_name += '.imagewall-term-' + container.terms[i];
    }
    var output = '';
    output += 'width: ' + container.state.width + ';\n';
    output += 'height: ' + container.state.height + ';\n';
    output_array[class_name] = output;

    for (var object_id in container['objects']) {
      var object = container['objects'][object_id];
      // Refresh state.
      imagewall_set_state($('#' + object_id), object, object.state.canvas);

      var class_name =  '.imagewall-canvas-object-' + object.field_name;
      for (var i = 0; i < container.terms.length; i++) {
        class_name += '.imagewall-term-' + container.terms[i];
      }
      var output = '';
      output += 'left: ' + object.state.x + ';\n';
      output += 'top: ' + object.state.y + ';\n';
      output += 'width: ' + object.state.width + ';\n';
      output += 'height: ' + object.state.height + ';\n';
      output_array[class_name] = output;
    }
  }

  output = '';
  for (var class_name in output_array) {
    output += class_name + ' {\n';
    output += output_array[class_name];
    output += '}\n';
  }
  $('#imagewall-template-output').text(output);
}