<?php
function imagewall_save_canvas() {
  $view_name  = $_REQUEST['view_name'];
  $view_id    = $_REQUEST['view_id'];
  $view_args  = $_REQUEST['view_args'];
  $containers = $_REQUEST['containers'];
  $title      = 'ImageWall Structure ('. $view_name .'/'. $view_args .')';
  $log        = $_REQUEST['log'];
  $timestamp  = time();

  // Create new revision.

  // Get nid for {views_args, view_id}
  $res = db_query("SELECT nid FROM {imagewall_nodes} WHERE view_id=%d AND view_arg='%s'", $view_id, $view_args);
  $item = db_fetch_object($res);
  $nid = $item->nid;

  // Create new node revision.
  $node = new StdClass();
  // creating a bare node
  $node->type = 'imagewall_structure';
  $node->nid = $nid;
  $node->revision = TRUE;
  $node->status = 1;
  $node->title = $title;
  $node->log = $log;
  node_save($node);
  
  if (empty($nid)) {
    // This is the first time this structure gets saved.
    db_query("INSERT INTO {imagewall_nodes}(nid, view_id, view_arg) VALUES(%d, '%d', '%s')", $node->nid, $view_id, $view_args);
  }

  // The node version id becomes the structure version ID (svid).
  $svid = $node->vid;

  // $bid = base field ID.
  // Save states of all containers and objects to database.
  foreach ($containers AS $bid => $container) {
    $state = $container['state'];
    imagewall_save($svid, $bid, 'container', $state['canvas'], $state['x'], $state['y'], $state['z'], $state['width'], $state['height']);

    foreach ($container['objects'] AS $field_name => $object) {
      $state = $object['state'];
      $item_id = imagewall_save($svid, $bid, $field_name, $state['canvas'], $state['x'], $state['y'], $state['z'], $state['width'], $state['height']);
    }
  }

  // Prepare the new revision for output.
  $rev = new StdClass();
  $rev->timestamp = $node->timestamp;
  $rev->log       = $log;
  $return = array(
    'svid'     => $svid,
    'title'    => imagewall_revision_title($rev),
    'string'   => imagewall_revision_string($rev),
  );

  return drupal_json($return);
}

/**
 * Retrieve list of all node revisions.
 *
 * @return
 *   Revisions in JSON format.
 */
function imagewall_retrieve_revisions() {
  $vid = $_REQUEST['vid'];

  $res   = db_query("SELECT nid FROM {node_revisions} WHERE vid=%d", $vid);
  $item = db_fetch_object($res);
  $nid = $item->nid;

  $node = new StdClass();
  $node->nid = $nid;
  $revisions_list = node_revision_list($node);

  // Prepare revisions list for output.
  $revisions = array();
  foreach ($revisions_list AS $vid => $rev) {
    $revisions[] = array(
      'vid'   => $vid,
      'string' => imagewall_revision_string($rev),
      'title'  => imagewall_revision_title($rev),
    );
  }
  
  return drupal_json(array('revisions' => $revisions));
}

/**
 * Load a certain node revision.
 */
function imagewall_load_revision() {
  $vid        = $_REQUEST['vid'];
  $view_name  = $_REQUEST['view_name'];
  $display_id = $_REQUEST['view_display'];

  //$result = db_query("SELECT nid FROM {node_revisions} WHERE vid=%d", $vid);
  //$item = db_fetch_object($result);
  //$node = node_load(array('nid' => $item->nid), $vid);
  
  // Check if this is latest revision.
  $latest = FALSE;
  $res   = db_query("SELECT nid FROM {node} WHERE vid=%d", $vid);
  $item = db_fetch_object($res);
  if (!empty($item->nid)) {
    $latest = TRUE;
  }

  // We load this node though the view,
  // because we need all fields to be formatted with the saved configuration.
  $view = views_get_view($view_name);
  $view->set_display($display_id);
  $view->display_handler->set_option('arguments', array(
    'vid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
    'style_options' => array(),
    'wildcard' => 'all',
    'wildcard_substitution' => 'All',
    'title' => '',
    'default_argument_type' => 'fixed',
    'default_argument' => '',
    'validate_type' => 'none',
    'validate_fail' => 'not found',
    'break_phrase' => 1,
    'not' => 0,
    'id' => 'vid',
    'table' => 'node_revisions',
    'field' => 'vid',
    'relationship' => 'none',
    'default_options_div_prefix' => '',
    'default_argument_user' => 0,
    'default_argument_fixed' => $vid,
    'default_argument_php' => '',
    'validate_argument_node_type' => array(
      'image' => 0,
      'object' => 0,
      'page' => 0,
      'story' => 0,
    ),
    'validate_argument_node_access' => 0,
    'validate_argument_nid_type' => 'nid',
    'validate_argument_vocabulary' => array(),
    'validate_argument_type' => 'tid',
    'user_argument_type' => '',
    'restrict_user_roles' => 0,
    'user_roles' => array(),
    'validate_argument_php' => '',
      ),
  ));
  $view->execute();

  // Prepare the node for output.
  $node = array();
  foreach ($view->field AS $field) {
    $node[$field->real_field] = $field->render($view->result[0]);
  }

  $data = array(
    'latest' => $latest,
    'node'   => $node,
  );

  return drupal_json($data);
}

/**
 * Save structure to database.
 *
 * @param $svid
 * @param $bid
 * @param $field_name
 * @param $canvas
 * @param $x
 * @param $y
 * @param $z
 * @param $width
 * @param $height
 */

function imagewall_save($svid, $bid, $field_name, $canvas, $x, $y, $z, $width, $height) {
  db_query("INSERT INTO {imagewall_structure}(svid, bid, field_name, canvas, x, y, z, width, height) VALUES(%d, %d, '%s', %d, %d, %d, %d, %d, %d)", $svid, $bid, $field_name, $canvas, $x, $y, $z, $width, $height);
}
?>