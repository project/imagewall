// Check js availability.
if (Drupal.jsEnabled) {
  // Start at onload-event.
  $(document).ready(imagewall_load);
}

/**
 * Load containers and objects.
 */
function imagewall_load() {
  $imagewall_board = $("#imagewall-canvas");
  $imagewall_box   = $("#imagewall-box");
  
  if (Drupal.settings.imagewall.containers) {
    var containers = Drupal.settings.imagewall.containers;

    for (var container_id in containers) {
      var $container = $('#' + container_id);

      // States.
      var container  = Drupal.settings.imagewall.containers[container_id];
      var state = container.state;
      
      if(state.canvas == true) {
        imagewall_attach_to_canvas($container, true);
        imagewall_init_state($container, state);
      }
    }
    if (Drupal.settings.imagewall.preview == true) {
      // First set preview=false, so it will be set to true when imagewall_preview is processed.
      Drupal.settings.imagewall.preview = false;
      imagewall_preview();
    }
  }
}

/**
 * A generic item has been dropped on the canvas.
 
 * This function differs between container and object.
 */
function imagewall_attach_to_canvas($item, init) {
  var container = Drupal.settings.imagewall.containers[$item.get(0).id];

  if (container) {
    // A container has been dropped.
    var default_settings = Drupal.settings.imagewall.default_settings;
    var $container = $item;

    if ($container.parent().get(0).id != 'imagewall-canvas') {
      // The container comes from the box.

      $container
      // Change position.
      .css({
        'position': 'absolute',
      })
      .appendTo($imagewall_board)
      // Change css class.
      .removeClass('imagewall-box-container')
      .addClass('imagewall-canvas-container')
      .css({
        'width':     null,
        'height':    null
      });

      // Assign templates.
      imagewall_container_assign_templates($container, container);

      // Prepare all contained objects and reset its states.
      var objects = container['objects'];
      /*
      var default_x = default_settings.object.canvas.x;
      var default_y = default_settings.object.canvas.y;
      */
      for (var object_id in objects) {
        var obj   = objects[object_id];
        var state = obj.state;
        var $obj  = $('#' + object_id);

        if (init == false) {
          // Reset object.
          state = {
            'canvas': true,
            'x'     : false,//default_x,
            'y'     : false,//default_y,
            'z'     : false,//default_settings.object.canvas.z,
            'width' : false,//default_settings.object.canvas.width,
            'height': false//default_settings.object.canvas.height
          };
        }

        // Assign template before calculating ratio and before initialization.
        imagewall_object_assign_templates($obj, container, obj);

        // Set ratio if object contains an image.
        var ratio = false;
        $obj.find('img:first').each(function(i) {
          // @todo: IE always says width=height=0. Thus ratio will be = NaN.
          var width, height;
          if ($obj.css('width')) width = parseInt($obj.css('width'));
          else                   width = 0;
          if ($obj.css('height')) height = parseInt($obj.css('height'));
          else                    height = 0;
          if (width > 0 && height > 0) {
            ratio = width / height;
          }
        });

        // Initialize state.
        imagewall_init_state($obj, state);

        $obj.fadeIn();
      }

      //$('.ui-resizable-e').addClass('imagewall-ui-resizable-e');
      //$('.ui-resizable-s').addClass('imagewall-ui-resizable-s');
      $('.ui-resizable-se').addClass('imagewall-ui-resizable-se');
    }
  }
}

/**
 * Initialize the current state.
 */
function imagewall_init_state($obj, state) {
  // Initialize with current states.
  $obj
  .css('left', state.x !== false ? (state.x + 'px') : null)
  .css('top', state.y !== false ? (state.y + 'px') : null)
  .css('z-index', state.z !== false ? (state.z + 'px') : null)
  .css('width', state.width !== false ? (state.width + 'px') : null)
  .css('height', state.height !== false ? (state.height + 'px') : null);

  if (Drupal.settings.imagewall.containers[$obj.get(0).id]) {
    // A container.

  }
  else {
    // An object.

    // Initialize dimensions.
    $obj.find('img')
    .css('width', $obj.css('width'))
    .css('height', $obj.css('height'));

    // Initialize text size.
    /*
    if ($obj.find('img').size() == 0) {
      $obj
      .css('font-size', state.height + 'px');
    }
    */
  }
}

function imagewall_preview() {
  preview = Drupal.settings.imagewall.preview;
  if (preview) {
    $('.imagewall-container-drag-handle').css('display', 'inherit');
    $('.imagewall-ui-resizable-se').css('display', 'inherit');
    Drupal.settings.imagewall.preview = false;
  }
  else {
    $('.imagewall-container-drag-handle').css('display', 'none');
    $('.imagewall-ui-resizable-se').css('display', 'none');
    Drupal.settings.imagewall.preview = true;
  }
}

function imagewall_object_assign_templates($obj, container, obj) {
  $obj.addClass('imagewall-canvas-object-' + obj.field_name);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.addClass('imagewall-term-' + container.terms[i]);
  }
}

function imagewall_object_remove_templates($obj, container, obj) {
  $obj.removeClass('imagewall-canvas-object-' + obj.field_name);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.removeClass('imagewall-term-' + container.terms[i]);
  }
}

function imagewall_container_assign_templates($obj, container) {
  $obj.addClass('imagewall-canvas-container-' + container.group);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.addClass('imagewall-term-' + container.terms[i]);
  }
  // Remove template from box.
  $obj.removeClass('imagewall-box-container-' + container.group);
}

function imagewall_container_remove_templates($obj, container) {
  $obj.removeClass('imagewall-canvas-container-' + container.group);
  for (var i = 0; i < container.terms.length; i++) {
    $obj.removeClass('imagewall-term-' + container.terms[i]);
  }
  // Assign template for box.
  $obj.addClass('imagewall-box-container-' + container.group);
}