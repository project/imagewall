<?php

/**
 * @file
 * Draggableviews processing functions.
 * Rough summary of what functions in this file do:
 *  - Fetch views-style-plugin information
 *  - Analyze structure
 *  - Build/rebuild hierarchy
 */

function imagewall_revision_string($rev) {
  return date('d.m.y, H:i', $rev->timestamp);
}

function imagewall_revision_title($rev) {
  return date('d.m.Y, H:i:s', $rev->timestamp) .': '. $rev->log;
}