<?php
/**
 * @file
 * Template to display a view as a draggable table.
 *
*/
?>
<table id="imagewall-table">
  <tr>
    <td width="120" valign="top" style="display:none">
      <table>
        <tr>
          <td style="padding-right: 5px;"><div id="imagewall-box">
<?php foreach ($containers AS $container_id => $container) { ?>
<div id="<?php echo $container_id; ?>" class="imagewall-box-container imagewall-box-container-<?php echo $container['group']; ?>">
<div class="imagewall-container-drag-handle<?php echo (!$container['latest'] ? ' imagewall-not-up-to-date' : ''); ?>"><?php echo $container['title']; ?></div>
<?php foreach ($container['objects'] AS $object_id => $object) { ?>
<div id="<?php echo $object_id; ?>" class="imagewall-canvas-object" style="display: none;">
<?php echo $object['content']; ?>
</div>
<?php } ?>
</div>
<?php } ?>
</div>
          </td>
        </tr>
        <tr>
          <td><a href="javascript:imagewall_preview()" id="imagewall-preview">Preview ON</a></td>
        </tr>
        <tr>
          <td><ul id="imagewall-selected-items">
            <li><?php echo t('Keep left mouse button pressed to open a selecting window. Select some container to change their revisions.'); ?></li>
          </ul></td>
        </tr>
      </table>
    </td>
    <td valign="top"><div id="imagewall-canvas"></div></td>
  </tr>
</table>